---
title: "Materials Chemistry Horizons"
layout: "home.njk"
class: "home"
permalink: "/"
---



Materials Chemistry Horizons publishes high-quality articles in the broad field of science and technology in all fields of materials and chemistry. Topics covered in the journal include but not limited to design, synthesis, characterization, and application of materials in:

Medicinal Chemistry and biomedical engineering (drug/gene delivery, cancer therapy, tissue engineering, diagnosis and bioimaging, electrochemical detection, and bio-sensing)

Energy generation and storage (fuel cells, solar cells, magnetic and semiconductor materials, water splitting, hydrogen production, and storage)

Food chemistry

Environmental chemistry (sensors, catalysts, membranes, surface modifications and coatings, corrosion and degradation, food packaging, and water treatment)

Green chemistry

Nanomaterials

3D and 4D printing
