---
title: Contact Us
layout: contact.njk
permalink: /contact/
---



## Get in Touch

Contact the journal using the below forms or connect with us via email, WhatsApp, or Telegram below.

Email: contact@universci.com

https://api.whatsapp.com/message/L4ZMQRLU3IROE1?autoload=1&app_absent=0

https://t.me/universci